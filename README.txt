SCALD FILE
---------------------------------

INTRODUCTION
------------

Scald Webform is a webform provider for the scald module. 
It allows you to drag and drop webforms in wysiwyg.

INSTALLATION
------------

Scald Webform depends on the following modules:
- Scald 
- Webform
